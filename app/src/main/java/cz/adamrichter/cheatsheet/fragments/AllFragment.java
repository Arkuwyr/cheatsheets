package cz.adamrichter.cheatsheet.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.adamrichter.cheatsheet.AppConfig;
import cz.adamrichter.cheatsheet.R;
import cz.adamrichter.cheatsheet.activities.DisplayCheatSheetActivity;
import cz.adamrichter.cheatsheet.workers.AppController;
import cz.adamrichter.cheatsheet.workers.DividerItemDecoration;
import cz.adamrichter.cheatsheet.workers.DownloadFileFromURL;
import cz.adamrichter.cheatsheet.workers.FilesHandler;
import cz.adamrichter.cheatsheet.workers.SQLiteHandler;
import cz.adamrichter.cheatsheet.workers.adapters.AllCheatSheetsListAdapter;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetMenuItem;
import cz.adamrichter.cheatsheet.workers.listeners.CheatSheetListTouchListener;

import static cz.adamrichter.cheatsheet.AppConfig.SCHOOL_SUBJECT;
import static cz.adamrichter.cheatsheet.activities.MainActivity.PERMISSION_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AllFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AllFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AllFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String filter = "all";


    private AllFragment.OnFragmentInteractionListener mListener;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private AllCheatSheetsListAdapter mAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<CheatSheetMenuItem> allCheatSheets = new ArrayList<>();
    private SQLiteHandler db;
    private HashMap<String, String> user;

    public final static String ACTIVITY_NAME_MESSAGE = "activityName";
    public final static String FILE_CHEAT_SHEET_NAME_MESSAGE = "fileName";

    public AllFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AllFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AllFragment newInstance(String param1, String param2) {
        AllFragment fragment = new AllFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_all, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_layout);
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity().getBaseContext(), LinearLayoutManager.VERTICAL));
        setTouchListener();
        setSwipeListener();



        db = new SQLiteHandler(getActivity().getBaseContext());
        user = db.getUserDetails();
        updateCheatSheets(filter);

        mAdapter = new AllCheatSheetsListAdapter(allCheatSheets);
        mRecyclerView.setAdapter(mAdapter);

        initAdvert(view);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void initAdvert(View view){
        AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void setSwipeListener() {
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        updateOperation(filter);
                    }
                }
        );
    }

    public void updateOperation(String type) {
        filter = type;
        mAdapter.clear();
        updateCheatSheets(type);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void setTouchListener() {
        mRecyclerView.addOnItemTouchListener(new CheatSheetListTouchListener(getActivity().getBaseContext(), mRecyclerView, new CheatSheetListTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (allCheatSheets.get(position).isDownloaded()) {
                    openCheatSheetActivity(allCheatSheets.get(position).getFileName(), allCheatSheets.get(position).getName());
                }else {
                    new DownloadFileFromURL(view).execute(allCheatSheets.get(position));
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final CheatSheetMenuItem item = allCheatSheets.get(position);
                builder.setMessage(item.getName()).setTitle(R.string.delete_cheat_sheet);
                builder.setPositiveButton(R.string.action_settings_stopwatch_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FilesHandler fh = new FilesHandler();
                        fh.deleteFile(item.getFileName());
                        updateOperation(filter);
                    }
                });
                builder.setNegativeButton(R.string.action_settings_stopwatch_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }}));
    }

    private void updateCheatSheets(String type) {
        if (!PERMISSION_EXTERNAL_STORAGE){
            return;
        }
        String tag_string_req = "req_login";


        final String chType = type;
        final String userToken = user.get("access_token");
        final FilesHandler fh = new FilesHandler();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GET_CHEAT_SHEETS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    Log.d("Compere", response.toString());
                    // Check for error node in json
                    if (!error) {
                        JSONArray cheatSheets = jObj.getJSONArray("cheat_sheets");
                        Log.d("download", cheatSheets.toString());
                        for (int i = 0; i < cheatSheets.length(); i++) {
                            JSONObject jsonobject = cheatSheets.getJSONObject(i);
                            String fileName = jsonobject.getString("location").split("/")[2].split("\\.")[0];

                            int subject = jsonobject.getInt("school_subject");

                            String name = jsonobject.getString("name");
                            String location = jsonobject.getString("location");
                            if (fh.isFileExist(fileName)){
                                allCheatSheets.add(new CheatSheetMenuItem(name,SCHOOL_SUBJECT[subject-1],true,location,fileName));
                            }else {
                                allCheatSheets.add(new CheatSheetMenuItem(name,SCHOOL_SUBJECT[subject-1],false,location,fileName));
                            }
                        }

                        mRecyclerView.setAdapter(new AllCheatSheetsListAdapter(allCheatSheets));
                        mRecyclerView.invalidate();

                    } else {
                        Log.d("Compere", "error");
                        // Error in login. Get the error message
                       /* String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();*/
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();/*
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();*/
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /*
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();*/
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("access_token", userToken);
                params.put("type", chType);


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void openCheatSheetActivity(String fileName, String cHName) {
        Intent intent = new Intent(getActivity(), DisplayCheatSheetActivity.class);
        intent.putExtra(ACTIVITY_NAME_MESSAGE, cHName);
        intent.putExtra(FILE_CHEAT_SHEET_NAME_MESSAGE, fileName);
        startActivity(intent);
    }
}
