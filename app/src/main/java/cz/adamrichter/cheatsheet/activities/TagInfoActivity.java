package cz.adamrichter.cheatsheet.activities;

import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import cz.adamrichter.cheatsheet.R;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetChapter;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetList;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetMathFormula;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetTerm;

public class TagInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MultiDex.install(this);
        setContentView(R.layout.activity_tag_info);
        initAdvert();

        setTitle(R.string.title_activity_tag_info);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView t1 = (TextView) findViewById(R.id.credits_biology);
        t1.setMovementMethod(LinkMovementMethod.getInstance());
        TextView t2 = (TextView) findViewById(R.id.credits_math);
        t2.setMovementMethod(LinkMovementMethod.getInstance());
        TextView t3 = (TextView) findViewById(R.id.credits_physics);
        t3.setMovementMethod(LinkMovementMethod.getInstance());
        TextView t4 = (TextView) findViewById(R.id.credits_chemistry);
        t4.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void initAdvert(){
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
}
