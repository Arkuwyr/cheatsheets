package cz.adamrichter.cheatsheet.activities;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.CountDownTimer;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cz.adamrichter.cheatsheet.fragments.AllFragment;
import cz.adamrichter.cheatsheet.workers.adapters.ExpandableListAdapter;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheet;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetItem;
import cz.adamrichter.cheatsheet.R;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetTerm;

import static android.view.View.FIND_VIEWS_WITH_TEXT;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class DisplayCheatSheetActivity extends AppCompatActivity{


    private CheatSheet ch;
    private ExpandableListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ExpandableListAdapter mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private String fileName;
    private String chName;
    private ScrollView sw;
    private ViewGroup layout;
    private int openedSubmenu =-1;
    private int currentQuery = 0;
    private List<Integer> locationOfQuery = new ArrayList<Integer>();
    private CountDownTimer stopwatch;
    private MenuItem stopWatchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_cheat_sheet);


        Intent intent = getIntent();
        fileName = intent.getStringExtra(AllFragment.FILE_CHEAT_SHEET_NAME_MESSAGE);
        chName = intent.getStringExtra(AllFragment.ACTIVITY_NAME_MESSAGE);
        layout = (ViewGroup) findViewById(R.id.cheatSheetContent);
        sw = (ScrollView) findViewById(R.id.cheatSheetScrollView);

        getSupportActionBar().setTitle(chName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        ch = new CheatSheet(fileName);

        mDrawerList = (ExpandableListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.display_cheat_sheet_activity);

        addDrawerItems();
        setupDrawer();
        setNavButtons();

        generateFullCheatSheetView();

        sw.setSmoothScrollingEnabled(true);

    }


    private void setNavButtons(){

        Button buttonNext = (Button) findViewById(R.id.next_usage_button);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(currentQuery < locationOfQuery.size()-1){
                    currentQuery++;
                    lookAtView(locationOfQuery.get(currentQuery));
                }

            }
        });

        Button buttonPrev = (Button) findViewById(R.id.previous_usage_button);
        buttonPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(0 < currentQuery){
                    currentQuery--;
                    lookAtView(locationOfQuery.get(currentQuery));
                }

            }
        });

    }

    private void addDrawerItems() {
        String[] osArray ;
        if (ch.haveElements()){
            osArray = ch.getItemsNames();
        }else{
            osArray = new String[1];
            osArray[0] = "None";
        }

        mAdapter = new ExpandableListAdapter(this, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                final int childPos = layout.getChildAt(getChildPositionOfName(v)).getTop();
                sw.post(new Runnable() {
                    public void run() {
                        sw.smoothScrollTo(0,childPos);
                    }
                });
                mDrawerLayout.closeDrawers();
                closeSubmenu(groupPosition);
                LinearLayout item = (LinearLayout)layout.getChildAt(getChildPositionOfName(v));
                ObjectAnimator colorAnim = ObjectAnimator.ofInt(item.getChildAt(0), "textColor",
                        getResources().getColor(R.color.highlight_cheat_sheet_item), getResources().getColor(R.color.item_text_color));
                colorAnim.setEvaluator(new ArgbEvaluator());
                colorAnim.setDuration(1000);
                colorAnim.start();
                return true;
            }
        });

        mDrawerList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                closeSubmenu(groupPosition);
            }
        });

        mDrawerList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                openedSubmenu = -1;
            }
        });

    }

    private void closeSubmenu(int menu){
        if (menu>=0){
            mDrawerList.collapseGroup(openedSubmenu);
            openedSubmenu = menu;
        }

    }

    private int getChildPositionOfName(View view) {
        LinearLayout lv = (LinearLayout)view;
        TextView tView = (TextView) lv.getChildAt(0);
        String name = tView.getText().toString();
        for (int i =0; i<layout.getChildCount();i++){
            if (layout.getChildAt(i) instanceof LinearLayout){
                LinearLayout item = (LinearLayout)layout.getChildAt(i);
                TextView itemName = (TextView) item.getChildAt(0);
                if (name.equals(itemName.getText().toString())){
                    return i;
                }
            }
        }
        return 0;
    }

    private int getPositionOfView(View view) {
        TextView tView = (TextView) view;
        String name = tView.getText().toString();
        for (int i =0; i<layout.getChildCount();i++){
           if(layout.getChildAt(i) instanceof LinearLayout){
               LinearLayout item = (LinearLayout)layout.getChildAt(i);
               TextView itemName = (TextView) item.getChildAt(0);
               TextView itemContent;
               if(item.getChildAt(1) instanceof TextView){
                   itemContent= (TextView) item.getChildAt(1);
               }else{
                   itemContent =(TextView) item.getChildAt(0);
               }

               if ((name.equals(itemName.getText().toString()))||(name.equals(itemContent.getText().toString()))){
                   return i;
               }
           }
        }
        return 0;
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.alphabetical_list);

                mDrawerList.bringToFront();
                mDrawerLayout.requestLayout();
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(chName);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    //Drawler---------------------------------------------------------------------------------------
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        initSearch(menu);
        return  true;
    }


    private void initSearch(final Menu menu) {
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);


        MenuItem searchItem = menu.findItem(R.id.action_search);
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                openBottomMenu();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                clearAllHiglights();
                closeBottomMenu();
                return true;
            }
        });

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextChange(String newText)
            {

                ArrayList<View> foundViews = new ArrayList<>();
                layout.findViewsWithText(foundViews,newText, FIND_VIEWS_WITH_TEXT);
                locationOfQuery.clear();
                if(foundViews.size()> 0){
                    lookAtView(getPositionOfView(foundViews.get(0)));
                    currentQuery =0;
                    clearAllHiglights();

                    for (View v :foundViews){
                        colorText(newText, (TextView) v);
                        locationOfQuery.add(getPositionOfView(v));


                    }

                    for (int i =1; i <locationOfQuery.size();i++){
                        if(locationOfQuery.get(i-1)==locationOfQuery.get(i)){
                            locationOfQuery.remove(i);
                        }
                    }

                    for (int i =0; i <locationOfQuery.size();i++){

                    }
                }
                return true;
            }
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                ArrayList<View> foundViews = new ArrayList<>();
                layout.findViewsWithText(foundViews,query,FIND_VIEWS_WITH_TEXT);
                locationOfQuery.clear();
                if(foundViews.size()> 0){
                    lookAtView(getPositionOfView(foundViews.get(0)));
                    currentQuery =0;
                }
                searchView.clearFocus();
                menu.findItem(R.id.action_search).collapseActionView();
                closeBottomMenu();
                searchView.onActionViewCollapsed();
                return true;
            }



        };

        searchView.setOnSearchClickListener(new SearchView.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomMenu();
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                closeBottomMenu();
                return false;
            }
        });


        searchView.setOnQueryTextListener(textChangeListener);
    }

    private void lookAtView(int view) {
        sw.scrollTo(0, layout.getChildAt(view).getTop());
    }

    private void openBottomMenu() {
        LinearLayout bottomMenu =(LinearLayout)findViewById(R.id.bottom_menu);

        float height = convertDpToPixel(55f, getBaseContext());

        bottomMenu.animate().translationY(-height).withLayer();
    }

    private void closeBottomMenu() {
        LinearLayout bottomMenu =(LinearLayout)findViewById(R.id.bottom_menu);

        float height = convertDpToPixel(55f, getBaseContext());

        bottomMenu.animate().translationY(height).withLayer();
    }


    private static void findAllTextViews(ArrayList<View> result, View v) {
        if(v instanceof TextView){
            result.add(v);
        }else if(v instanceof LinearLayout) {
            for (int i =0; i < ((LinearLayout) v).getChildCount();i++){
                findAllTextViews(result,((LinearLayout) v).getChildAt(i));
            }
         }
    }

    private void clearAllHiglights(){
        ArrayList<View> allViews = new ArrayList<>();
        findAllTextViews(allViews,layout);

        for(View v :allViews){
            clearView((TextView)v);
        }
    }


    private void colorText(String newText, TextView tv) {

        SpannableString spannable = new SpannableString(tv.getText());
        String text = tv.getText().toString().toLowerCase();
        String textSplit[] = text.split(newText.toLowerCase());


        if(textSplit.length==0){
            spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.highlight_cheat_sheet_substring)),
                    0, newText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        int startPos =0;
        int endPos;

        for (int i =0; i<textSplit.length-1;i++){
            startPos +=textSplit[i].length();
            if(startPos+newText.length()<=text.length()){
                endPos = startPos+newText.length();
            }else{
                endPos = text.length();
            }

            spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.highlight_cheat_sheet_substring)),
                    startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            startPos += newText.length();
        }


        tv.setText(spannable, TextView.BufferType.SPANNABLE);
    }

    private void clearView(TextView tv) {

            SpannableString spanName = new SpannableString(tv.getText());
            ForegroundColorSpan[] toRemoveSpans= spanName.getSpans(0, tv.getText().toString().length(), ForegroundColorSpan.class);


            for (int i = 0; i < toRemoveSpans.length; i++) {
                spanName.removeSpan(toRemoveSpans[i]);
            }

        tv.setText(spanName, TextView.BufferType.SPANNABLE);

            //ForegroundColorSpan spanNam = new ForegroundColorSpan(getResources().getColor(R.color.highlight_cheat_sheet_item));
            //spanName.removeSpan(spanNam);

    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_stopwatch:
                openStopwatchMenu();
                stopWatchMenuItem = item;
                stopWatchMenuItem.setEnabled(false);
                return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //-------------------------------------------stopwatch------------------------------------------
    //----------------------------------------------------------------------------------------------


    private void openStopwatchMenu() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View stopwatchView = inflater.inflate(R.layout.set_stopwatch_layout, null);
        builder.setView(stopwatchView);

        final NumberPicker unit_minutes = (NumberPicker) stopwatchView.findViewById(R.id.minutes_picker);

        unit_minutes.setWrapSelectorWheel(false);
        unit_minutes.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        String minuts[] = new String[19];
        for(int i = 0;i <=90; i+=5) {
            if( i < 10 )
                minuts[i/5] = "0"+i;
            else
                minuts[i/5] = ""+i;
        }
        unit_minutes.setDisplayedValues(minuts);
        unit_minutes.setMinValue(0);
        unit_minutes.setMaxValue(minuts.length-1);

        builder.setPositiveButton(R.string.action_settings_stopwatch_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                setupStopwatch(unit_minutes.getValue());
                showStopWatch();

            }
        });
        builder.setNegativeButton(R.string.action_settings_stopwatch_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                stopWatchMenuItem.setEnabled(true);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setupStopwatch( int time) {
        final int millisTime = time*60*5*1000;

        final TextView stopwatchDisplay = (TextView) findViewById(R.id.stop_watch_display);
        stopwatchDisplay.setText(getResources().getString(R.string.stopwatch_display,
                TimeUnit.MILLISECONDS.toMinutes( millisTime),
                TimeUnit.MILLISECONDS.toSeconds(millisTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisTime))));

        stopwatchDisplay.setTextColor(getResources().getColor(R.color.item_text_color));
        stopwatchDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopwatch.start();
                ObjectAnimator.ofObject(stopwatchDisplay,"textColor",new ArgbEvaluator(),getResources().getColor(R.color.stop_watch_start),
                        getResources().getColor(R.color.stop_watch_end)).setDuration(millisTime).start();
            }
        });


        stopwatch = new CountDownTimer(millisTime, 1000) {
            public void onTick(long millisUntilFinished) {
                stopwatchDisplay.setText(getResources().getString(R.string.stopwatch_display,
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                Animation pulse = AnimationUtils.loadAnimation(getBaseContext(), R.anim.pulsestopwatch);

                pulse.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        closeStopWatch();
                        stopWatchMenuItem.setEnabled(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                stopwatchDisplay.startAnimation(pulse);
            }

        };
    }

    private void showStopWatch() {
        LinearLayout bottomMenu =(LinearLayout)findViewById(R.id.right_menu);

        float width = convertDpToPixel(130f, getBaseContext());

        bottomMenu.animate().translationX(-width).withLayer();
    }

    private void closeStopWatch() {
        LinearLayout bottomMenu =(LinearLayout)findViewById(R.id.right_menu);

        float width = convertDpToPixel(130f, getBaseContext());

        bottomMenu.animate().translationX(width).withLayer();
    }

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    private void generateFullCheatSheetView() {
        if (ch.haveElements()){
            for (CheatSheetItem item : ch.getAplhabeticalItems()) {
                layout.addView(item.createDisplayedPart(getBaseContext(), getResources()));
                addSeparator();
            }
        }else{
            CheatSheetTerm term = new CheatSheetTerm();
            term.setName("None");
            term.setContent("Cheat sheet has parse error or is empty. Visit info about making cheat sheets.");
            layout.addView(term.createDisplayedPart(getBaseContext(), getResources()));
        }
    }

    private void addSeparator(){
    View v = new View(getBaseContext());
    ViewGroup.LayoutParams vp = new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1);
    v.setBackgroundColor(getResources().getColor(R.color.item_separator));
    v.setLayoutParams(vp);
    layout.addView(v);
    }

}