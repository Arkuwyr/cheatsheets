package cz.adamrichter.cheatsheet.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.login.LoginManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.HashMap;

import cz.adamrichter.cheatsheet.Manifest;
import cz.adamrichter.cheatsheet.fragments.AllFragment;
import cz.adamrichter.cheatsheet.fragments.BiologyFragment;
import cz.adamrichter.cheatsheet.fragments.ChemistryFragment;
import cz.adamrichter.cheatsheet.fragments.MathFragment;
import cz.adamrichter.cheatsheet.fragments.MotherLanguageFragment;
import cz.adamrichter.cheatsheet.fragments.PhysicsFragment;
import cz.adamrichter.cheatsheet.workers.CircleTransform;
import cz.adamrichter.cheatsheet.R;
import cz.adamrichter.cheatsheet.workers.SQLiteHandler;
import cz.adamrichter.cheatsheet.workers.SessionManager;


public class MainActivity extends AppCompatActivity implements AllFragment.OnFragmentInteractionListener,BiologyFragment.OnFragmentInteractionListener, ChemistryFragment.OnFragmentInteractionListener, MathFragment.OnFragmentInteractionListener, MotherLanguageFragment.OnFragmentInteractionListener,PhysicsFragment.OnFragmentInteractionListener{

    private static int PERMISSIONS_REQUEST_EXTERNAL_STORAGE;
    public static boolean PERMISSION_EXTERNAL_STORAGE = false;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtWebsite;
    private Toolbar toolbar;

    // urls to load navigation header background image
    // and profile image
    private static String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";
    private static String urlProfileImg = "https://lh3.googleusercontent.com/eCtE_G34M9ygdkmOpYvCag1vBARCmZwnVS6rS5t4JLzJ6QgQSBquM0nuTsCpLhYbKljoyS-txg";

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_ALL_CHEAT_SHEETS = "allCheatSheets";
    private static final String TAG_BIOLOGY_CHEAT_SHEETS = "biologyCheatSheets";
    private static final String TAG_MATH_CHEAT_SHEETS = "mathCheatSheets";
    private static final String TAG_MOTHER_LANGUAGE_CHEAT_SHEETS = "motherLanguageCheatSheets";
    private static final String TAG_PHYSICS_CHEAT_SHEETS = "physicsCheatSheets";
    private static final String TAG_CHEMISTRY_CHEAT_SHEETS = "chemistryCheatSheets";
    private static final String TAG_SETTINGS = "settings";
    public static String CURRENT_TAG = TAG_ALL_CHEAT_SHEETS;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;

    private SQLiteHandler db;
    private SessionManager session;

    HashMap<String, String> user;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MultiDex.install(this);

        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        permRequest();
        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtWebsite = (TextView) navHeader.findViewById(R.id.website);
        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);

        db = new SQLiteHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        user = db.getUserDetails();

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_ALL_CHEAT_SHEETS;
            loadAllFragment();
        }
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-7090957625869112~4813408187");
    }

    private void permRequest(){
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

                PERMISSION_EXTERNAL_STORAGE = true;

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else{
            PERMISSION_EXTERNAL_STORAGE = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_EXTERNAL_STORAGE) {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PERMISSION_EXTERNAL_STORAGE = true;

                } else {
                    PERMISSION_EXTERNAL_STORAGE = false;
                }
                return;
            }

        }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        // name, website
        txtName.setText(user.get("name"));
        txtWebsite.setText(user.get("email"));
        urlProfileImg = user.get("image");

        // loading header background image
        Glide.with(this).load(urlNavHeaderBg)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);

        // Loading profile image
        Glide.with(this).load(urlProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);

        // showing dot next to notifications label
        //navigationView.getMenu().getItem(navItemIndex).setActionView(R.layout.menu_dot);
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadAllFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getAllFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }


        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getAllFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                AllFragment allFragment = new AllFragment();
                return allFragment;

            case 1:
                // biologie
                BiologyFragment biologyFragment = new BiologyFragment();
                return biologyFragment;
            case 2:
                // Matematika
                MathFragment mathFragment= new MathFragment();
                return mathFragment;
            case 3:
                // mateřský jazyk
                MotherLanguageFragment motherLanguageFragment= new MotherLanguageFragment();
                return motherLanguageFragment;
            case 4:
                // Fyzika
                PhysicsFragment physicsFragment= new PhysicsFragment();
                return physicsFragment;
            case 5:
                // chemie
                ChemistryFragment chemistryFragment = new ChemistryFragment();
                return chemistryFragment;
            default:
                return new AllFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_all_cheat_sheets:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_ALL_CHEAT_SHEETS;
                        break;
                    case R.id.nav_biology_cheat_sheets:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_BIOLOGY_CHEAT_SHEETS;
                        break;
                    case R.id.nav_math_cheat_sheets:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_MATH_CHEAT_SHEETS;
                        break;
                    case R.id.nav_mother_language_cheat_sheets:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_MOTHER_LANGUAGE_CHEAT_SHEETS;
                        break;
                    case R.id.nav_physics_cheat_sheets:
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_PHYSICS_CHEAT_SHEETS;
                        break;
                    case R.id.nav_chemistry_cheat_sheets:
                        navItemIndex = 5;
                        CURRENT_TAG = TAG_CHEMISTRY_CHEAT_SHEETS;
                        break;
                    /*
                    case R.id.nav_settings:
                        navItemIndex = 6;
                        CURRENT_TAG = TAG_SETTINGS;
                        break;*/
                    case R.id.nav_info:
                        // launch new intent instead of loading fragment
                        startActivity(new Intent(MainActivity.this, TagInfoActivity.class));
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_logout:
                        // launch new intent instead of loading fragment
                        logoutUser();
                        LoginManager.getInstance().logOut();
                        return true;
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadAllFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_ALL_CHEAT_SHEETS;
                loadAllFragment();
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.main, menu);
        }

        // when fragment is notifications, load the menu created for notifications
        if (navItemIndex == 3) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_display_all) {
            AllFragment f = (AllFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
            f.updateOperation("all");
            return true;
        }
        if (id == R.id.action_display_downloaded) {
            AllFragment f = (AllFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
            f.updateOperation("downloaded");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
