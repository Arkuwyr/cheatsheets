package cz.adamrichter.cheatsheet;

/**
 * Created by Adam on 18. 2. 2017.
 */

public class AppConfig {
    // Server download cheat sheets url
    public static String URL_TO_DOWNLOAD= "http://cheat-sheets-app.com/";
    // Server user login url
    public static String URL_LOGIN = URL_TO_DOWNLOAD+"api/login";

    // Server user register url
    public static String URL_REGISTER = URL_TO_DOWNLOAD+"api/register";

    // Server get cheat sheets url
    public static String URL_GET_CHEAT_SHEETS= URL_TO_DOWNLOAD+"api/get-cheat-sheets";
    // Server login with facebook
    public static String URL_FB_LOGIN= URL_TO_DOWNLOAD+"api/fb-login";

    public static String[] SCHOOL_SUBJECT =  {"Biologie","Matematika", "Český jazyk", "Fyzika", "Chemie"};
}
