package cz.adamrichter.cheatsheet.workers.cheatsheetelements;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;

/**
 * Created by Adam on 15. 1. 2017.
 */

public interface CheatSheetItem {
    String getName();
    void setName(String name);
    void setContent(String content);
    View createDisplayedPart(Context context, Resources resources);
}
