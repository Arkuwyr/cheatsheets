package cz.adamrichter.cheatsheet.workers.cheatsheetelements;

/**
 * Created by Adam on 11. 3. 2017.
 */

public class CheatSheetMenuItem {

    private String name;
    private String type;
    private boolean downloaded;
    private String url;
    private String fileName;

    public CheatSheetMenuItem(String name, String type, boolean downloaded, String url, String fileName){
        this.name = name;
        this.type = type;
        this.downloaded = downloaded;
        this.url = url;
        this.fileName = fileName;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public boolean isDownloaded() {
        return downloaded;
    }

    public String getUrl() {
        return url;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
