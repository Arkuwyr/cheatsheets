package cz.adamrichter.cheatsheet.workers;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.CheckedTextView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import cz.adamrichter.cheatsheet.R;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetMenuItem;

import static cz.adamrichter.cheatsheet.AppConfig.URL_TO_DOWNLOAD;

/**
 * Created by Adam on 11. 3. 2017.
 */

public class DownloadFileFromURL extends AsyncTask<CheatSheetMenuItem, String, String> {

    public RelativeLayout view;
    public ProgressBar progressBar;
    public TextView progressText;
    CheckedTextView checkedTextView;
    private FilesHandler fh;

    public DownloadFileFromURL(View view) {

        this.view = (RelativeLayout) view;
        fh= new FilesHandler();
    }

    /**
     * Before starting background thread
     * Show Progress Bar Dialog
     * */



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = (ProgressBar) view.findViewById(R.id.download_progress);
        progressText = (TextView) view.findViewById(R.id.download_percentage);
        checkedTextView = (CheckedTextView) view.findViewById(R.id.image_download_check);
        progressBar.setAlpha(1);
        progressText.setAlpha(1);

    }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected String doInBackground(CheatSheetMenuItem... item) {
        int count;
        try {
            String[] name =item[0].getUrl().split("/");
            URL url = new URL(URL_TO_DOWNLOAD+"/"+name[0]+"/"+ name[1]+"/"+ URLEncoder.encode(name[2] ,"UTF-8"));
            URLConnection conection = url.openConnection();
            conection.connect();
            // getting file length
            int lenghtOfFile = conection.getContentLength();
            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream to write file
            OutputStream output = new FileOutputStream(fh.cheatSheetFolderPath+"/"+name[2]);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress(""+(int)((total*100)/lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();
            Log.d("downloader",name[2]);
            item[0].setFileName(name[2].split("\\.")[0]);
            item[0].setDownloaded(true);
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
            e.printStackTrace();
        }


        return null;
    }

    /**
     * Updating progress bar
     * */
    protected void onProgressUpdate(String... progress) {
        progressBar.setProgress(Integer.parseInt(progress[0]));
        progressText.setText(view.getContext().getResources().getString(R.string.download_progress_info, progress[0]));
    }

    /**
     * After completing background task
     * Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String file_url) {
        AlphaAnimation animation1 = new AlphaAnimation(1.0f, 0.0f);
        animation1.setDuration(750);
        progressBar.startAnimation(animation1);
        progressText.startAnimation(animation1);

        checkedTextView.setChecked(true);
    }

}
