package cz.adamrichter.cheatsheet.workers.cheatsheetelements;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import cz.adamrichter.cheatsheet.workers.FilesHandler;

/**
 * Created by Adam on 15. 1. 2017.
 */

public class CheatSheet {

    private ArrayList<CheatSheetItem> items;
    private FilesHandler fh;
    private int numOfCheatSheetItem;

    public CheatSheet(String fileName) {
        fh = new FilesHandler();
        items = new ArrayList<>();
        String[] fileInfo = fh.readFromFile(fileName);
        if (fileInfo[0].equals("txt")){
            numOfCheatSheetItem = parseTxtFile(fileInfo[1]);
        }else if (fileInfo[0].equals("json")){
            try {
                numOfCheatSheetItem = parseJsonFile(fileInfo[1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public boolean haveElements(){
        if (numOfCheatSheetItem >0 ){
            return true;
        }
        return false;
    }

    private int parseTxtFile(String file) {

        //|T|name:aaaaa|content:bbbbb|
        //|L|name:aaaaa|content:bbbbb|
        //|CH|name:aaaaa|content:bbbbb|

        file = file.replace("\n", "").replace("\r", "");
        String[] parts = file.split(Pattern.quote("|"));
        for (int i = 0; i < parts.length; i++) {
            //pojem
            if (parts[i].equals("T")) {
                CheatSheetTerm term = new CheatSheetTerm();
                term.setName(parts[i + 1]);
                term.setContent(parts[i + 2]);
                items.add(term);
            }
            //seznam
            if (parts[i].equals("L")) {
                CheatSheetList term = new CheatSheetList();
                term.setName(parts[i + 1]);
                term.setContent(parts[i + 2]);
                items.add(term);
            }
            //kapitola
            if (parts[i].equals("CH")) {
                CheatSheetChapter term = new CheatSheetChapter();
                term.setName(parts[i + 1]);
                term.setContent(parts[i + 2]);
                items.add(term);
            }
            if (parts[i].equals("MF")) {
                CheatSheetMathFormula term = new CheatSheetMathFormula();
                term.setName(parts[i + 1]);
                term.setContent(parts[i + 2]);
                items.add(term);
            }
        }


        return items.size();
    }


    private int parseJsonFile(String fileContent) throws JSONException {
        JSONArray itemsJSON = new JSONArray(fileContent);

        for (int i = 0;i <itemsJSON.length();i++){
            String type = itemsJSON.getJSONObject(i).getString("type");
            //pojem
            if(type.equals("therm")){
                CheatSheetTerm term = new CheatSheetTerm();
                term.setName(itemsJSON.getJSONObject(i).getString("name"));
                term.setContent(itemsJSON.getJSONObject(i).getJSONArray("content").getString(0));
                items.add(term);
            }
            //seznam
            if(type.equals("list")){
                CheatSheetList list = new CheatSheetList();
                list.setName(itemsJSON.getJSONObject(i).getString("name"));
                JSONArray lItemsJSON = itemsJSON.getJSONObject(i).getJSONArray("content");
                for (int p = 0;p <lItemsJSON.length();p++){
                    list.addContent(lItemsJSON.getString(p));
                }
                items.add(list);
            }
            //vzorec
            if (type.equals("math")) {
                CheatSheetMathFormula term = new CheatSheetMathFormula();
                term.setName(itemsJSON.getJSONObject(i).getString("name"));
                term.setContent(itemsJSON.getJSONObject(i).getJSONArray("content").getString(0));
                items.add(term);
            }
            if (type.equals("chapter")) {
                CheatSheetChapter term = new CheatSheetChapter();
                term.setName(itemsJSON.getJSONObject(i).getString("name"));
                term.setContentJSon(itemsJSON.getJSONObject(i).getJSONArray("content"));
                items.add(term);
            }
        }

        return  items.size();

    }

    public ArrayList<CheatSheetItem> getAplhabeticalItems() {
        return items;
    }

    public String[] getItemsNames() {
        ArrayList<String> out = new ArrayList<>();
        for (CheatSheetItem item : items) {
            out.add(item.getName());
        }
        String[] outSort = out.toArray(new String[items.size()]);
        Arrays.sort(outSort);
        return outSort;
    }
}
