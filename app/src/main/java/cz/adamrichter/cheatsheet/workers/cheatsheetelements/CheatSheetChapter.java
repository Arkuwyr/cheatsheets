package cz.adamrichter.cheatsheet.workers.cheatsheetelements;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import cz.adamrichter.cheatsheet.R;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetItem;

/**
 * Created by Adam on 20. 1. 2017.
 */

public class CheatSheetChapter implements CheatSheetItem {

    private String name;
    private List<CheatSheetItem> content;

    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {this.name = name;
    }

    @Override
    public void setContent(String content) {
        String[] contentItems = content.split(":");
        this.content = new LinkedList<>();

        for (int i =0; i < contentItems.length;i++){

            if (contentItems[i].equals("T")) {
                CheatSheetTerm therm = new CheatSheetTerm();
                therm.setName(contentItems[i+1]);
                therm.setContent(contentItems[i+2]);
                this.content.add(therm);
            }
            //seznam
            if (contentItems[i].equals("L")) {
                CheatSheetList list = new CheatSheetList();
                list.setName(contentItems[i+1]);
                list.setContent(contentItems[i+2]);
                this.content.add(list);
            }
        }
    }
    public void setContentJSon(JSONArray array) throws JSONException {
        this.content = new LinkedList<>();
        for (int i = 0;i <array.length();i++){
            String type = array.getJSONObject(i).getString("type");
            //pojem
            if(type.equals("therm")){
                CheatSheetTerm term = new CheatSheetTerm();
                term.setName(array.getJSONObject(i).getString("name"));
                term.setContent(array.getJSONObject(i).getJSONArray("content").getString(0));
                content.add(term);
            }
            //seznam
            if(type.equals("list")){
                CheatSheetList list = new CheatSheetList();
                list.setName(array.getJSONObject(i).getString("name"));
                JSONArray lItemsJSON = array.getJSONObject(i).getJSONArray("content");
                for (int p = 0;p <lItemsJSON.length();p++){
                    list.addContent(lItemsJSON.getString(p));
                }
                content.add(list);
            }
            //vzorec
            if (type.equals("math")) {
                CheatSheetMathFormula term = new CheatSheetMathFormula();
                term.setName(array.getJSONObject(i).getString("name"));
                term.setContent(array.getJSONObject(i).getJSONArray("content").getString(0));
                content.add(term);
            }
        }
    }

    @Override
    public View createDisplayedPart(Context context, Resources resources) {


        LinearLayout gl = new LinearLayout(context);
        gl.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        gl.setLayoutParams(params);


        TextView name = new TextView(context);
        name.setTextSize(25);
        name.setPadding(70, 0, 0, 0);
        name.setBackgroundColor(resources.getColor(R.color.item_name));
        SpannableString spanString = new SpannableString(this.name);
        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        name.setText(spanString);
        name.setGravity(Gravity.CENTER);

        LinearLayout content = new LinearLayout(context);
        content.setPadding(40, 5, 15, 40);
        content.setBackgroundColor(resources.getColor(R.color.item_content));
        content.setLayoutParams(params);

        gl.addView(name);
        for (int i = 0; i < this.content.size(); i++) {
            gl.addView(this.content.get(i).createDisplayedPart(context,resources));

        }
        return gl;
    }
}
