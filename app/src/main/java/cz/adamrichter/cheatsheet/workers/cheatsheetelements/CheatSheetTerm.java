package cz.adamrichter.cheatsheet.workers.cheatsheetelements;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import cz.adamrichter.cheatsheet.R;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetItem;

/**
 * Created by Adam on 15. 1. 2017.
 */

public class CheatSheetTerm implements CheatSheetItem {
    private String name;
    private String content;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public View createDisplayedPart(Context context, Resources resources) {
        LinearLayout gl = new LinearLayout(context);
        gl.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        gl.setLayoutParams(params);

        TextView name = new TextView(context);
        name.setTextSize(17);
        name.setPadding(70, 0, 0, 0);
        name.setBackgroundColor(resources.getColor(R.color.item_name));
        name.setTextColor(context.getResources().getColor(R.color.item_text_color));
        SpannableString spanString = new SpannableString(this.name);
        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        name.setText(spanString);

        TextView content = new TextView(context);
        content.setTextSize(15);
        content.setPadding(40, 5, 15, 40);
        content.setBackgroundColor(resources.getColor(R.color.item_content));
        SpannableString spanString2 = new SpannableString(this.content);
        spanString2.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString2.length(), 0);
        content.setText(spanString2);
        content.setLayoutParams(params);
        content.setTextColor(context.getResources().getColor(R.color.item_text_color));


        gl.addView(name);
        gl.addView(content);
        return gl;
    }

}
