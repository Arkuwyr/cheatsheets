package cz.adamrichter.cheatsheet.workers.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.util.List;

import cz.adamrichter.cheatsheet.R;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetMenuItem;

/**
 * Created by adamr on 17. 2. 2017.
 */

public class AllCheatSheetsListAdapter extends RecyclerView.Adapter<AllCheatSheetsListAdapter.MyViewHolder>{

    private List<CheatSheetMenuItem> cheatSheetsList;
    public static final int ITEM_DOWNLOADED= 1;
    public static final int ITEM_NOT_DOWNLOADED= 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            year = (TextView) view.findViewById(R.id.year);
        }
    }


    public AllCheatSheetsListAdapter(List<CheatSheetMenuItem> cheatSheetsList) {
        this.cheatSheetsList = cheatSheetsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cheat_sheet_list_row, parent, false);
        CheckedTextView ctv = (CheckedTextView) itemView.findViewById(R.id.image_download_check);
        if (viewType == ITEM_DOWNLOADED) {
            ctv.setChecked(true);
        } else{
            ctv.setChecked(false);
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        if (cheatSheetsList.get(position).isDownloaded()) {
            return ITEM_DOWNLOADED;
        } else {
            return ITEM_NOT_DOWNLOADED;
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText(cheatSheetsList.get(position).getName());
        holder.year.setText(cheatSheetsList.get(position).getType());
}

    @Override
    public int getItemCount() {
        return cheatSheetsList.size();
    }

    public void clear() {
        int size = this.cheatSheetsList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.cheatSheetsList.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }
}