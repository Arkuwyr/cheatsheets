package cz.adamrichter.cheatsheet.workers.cheatsheetelements;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import cz.adamrichter.cheatsheet.R;

/**
 * Created by Adam on 7. 2. 2017.
 */

public class CheatSheetMathFormula implements CheatSheetItem {

    private String name;
    private String content;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name =name;
    }

    @Override
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public View createDisplayedPart(Context context, Resources resources) {
        LinearLayout gl = new LinearLayout(context);
        gl.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        gl.setLayoutParams(params);

        TextView name = new TextView(context);
        name.setTextSize(17);
        name.setPadding(70, 0, 0, 0);
        name.setBackgroundColor(resources.getColor(R.color.item_name));
        name.setTextColor(context.getResources().getColor(R.color.item_text_color));
        SpannableString spanString = new SpannableString(this.name);
        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        name.setText(spanString);


        WebView wv = new WebView(context);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setBackgroundColor(resources.getColor(R.color.item_name));
        wv.getSettings().setSupportZoom(false);
        String js = "<html><head>"
                + "<link rel='stylesheet' href='file:///android_asset/mathscribe/jqmath-0.4.3.css'>"
                + "<script src = 'file:///android_asset/mathscribe/jquery-1.4.3.min.js'></script>"
                + "<script src = 'file:///android_asset/mathscribe/jqmath-etc-0.4.6.min.js'></script>"
                + "<link rel='stylesheet' type='text/css' href='file:///android_asset/mathscribe/style.css'>"
                + "</head><body>$$\\cl 'mathFormula'{"+content+"}$$</a></body></html>";
        wv.loadDataWithBaseURL("",js,"text/html", "UTF-8", "");

        gl.addView(name);
        gl.addView(wv);
        return gl;
    }
}
