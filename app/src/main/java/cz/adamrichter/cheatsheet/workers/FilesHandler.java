package cz.adamrichter.cheatsheet.workers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Adam on 14. 1. 2017.
 */

public class FilesHandler {
    private String cheatSheetFolder;
    public String cheatSheetFolderPath;

    public FilesHandler() {
        cheatSheetFolder = "Cheat Sheets";
        initFolder();
    }

    public void initFolder() {
        File f = new File(Environment.getExternalStorageDirectory(), cheatSheetFolder);
        if (!f.exists()) {
            f.mkdirs();
        }
        cheatSheetFolderPath = f.getPath();
    }

    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_WORLD_READABLE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public boolean isFileExist(String name){
        File fTxt = new File(cheatSheetFolderPath+"/"+name+".txt");
        File fJSn = new File(cheatSheetFolderPath+"/"+name+".json");
        return ((fTxt.exists() && !fTxt.isDirectory())||(fJSn.exists() && !fJSn.isDirectory()));
    }

    public boolean deleteFile(String name){
        File fTxt = new File(cheatSheetFolderPath+"/"+name+".txt");
        File fJSn = new File(cheatSheetFolderPath+"/"+name+".json");
        if (fTxt.exists() && !fTxt.isDirectory()){
            return fTxt.delete();

        }
        if(fJSn.exists() && !fJSn.isDirectory()){
            return fJSn.delete();
        }
        return false;
    }

    public String[] readFromFile(String file) {
        StringBuffer datax = new StringBuffer("");

        String[] toReturn = new String[2];
        toReturn[0] ="none";

        File fTxt = new File(cheatSheetFolderPath+"/"+file+".txt");
        File fJson = new File(cheatSheetFolderPath+"/"+file+".json");
        if(fTxt.exists() && !fTxt.isDirectory()) {
            toReturn[0] ="txt";
            try {
                FileInputStream fIn = new FileInputStream(new File(cheatSheetFolderPath+"/"+file+".txt"));

                InputStreamReader isr = new InputStreamReader ( fIn,"UTF-8" ) ;
                BufferedReader buffreader = new BufferedReader ( isr ) ;

                String readString = buffreader.readLine ( ) ;
                while ( readString != null ) {
                    datax.append(readString);
                    readString = buffreader.readLine ( ) ;
                }

                isr.close ( ) ;
            } catch ( IOException ioe ) {
                ioe.printStackTrace ( ) ;
            }
        }else if(fJson.exists() && !fJson.isDirectory()) {
            toReturn[0] ="json";
            try {
                FileInputStream fIn = new FileInputStream(new File(cheatSheetFolderPath+"/"+file+".json"));

                InputStreamReader isr = new InputStreamReader ( fIn,"UTF-8" ) ;
                BufferedReader buffreader = new BufferedReader ( isr ) ;

                String readString = buffreader.readLine ( ) ;
                while ( readString != null ) {
                    datax.append(readString);
                    readString = buffreader.readLine ( ) ;
                }

                isr.close ( ) ;
            } catch ( IOException ioe ) {
                ioe.printStackTrace ( ) ;
            }
        }

        toReturn[1] =datax.toString();
        return toReturn;
        }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public List<File> getCheatSheetsList() {
        File parentDir = new File(Environment.getExternalStorageDirectory(), cheatSheetFolder);
        List<File> inFiles = new ArrayList<>();
        Queue<File> files = new LinkedList<>();
        files.addAll(Arrays.asList(parentDir.listFiles()));
        while (!files.isEmpty()) {
            File file = files.remove();
            if (file.isDirectory()) {
                files.addAll(Arrays.asList(file.listFiles()));
            } else if (file.getName().endsWith(".csv")) {
                inFiles.add(file);
            }
        }
        inFiles = Arrays.asList(parentDir.listFiles());
        return inFiles;
    }
}
