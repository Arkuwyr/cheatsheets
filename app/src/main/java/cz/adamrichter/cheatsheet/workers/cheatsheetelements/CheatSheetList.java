package cz.adamrichter.cheatsheet.workers.cheatsheetelements;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.LinkedList;

import cz.adamrichter.cheatsheet.R;
import cz.adamrichter.cheatsheet.workers.cheatsheetelements.CheatSheetItem;

/**
 * Created by Adam on 20. 1. 2017.
 */

public class CheatSheetList implements CheatSheetItem {

    private String name;
    private LinkedList<String> content;

    public CheatSheetList() {
        this.content= new LinkedList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    public void addContent(String content){
        this.content.add(content);
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setContent(String content) {
        this.content= new LinkedList<>(Arrays.asList(content.split("-")));
        this.content.removeFirst();
    }

    @Override
    public View createDisplayedPart(Context context, Resources resources) {
        LinearLayout gl = new LinearLayout(context);
        gl.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        gl.setLayoutParams(params);

        TextView name = new TextView(context);
        name.setTextSize(17);
        name.setPadding(70, 0, 0, 0);
        name.setBackgroundColor(resources.getColor(R.color.item_name));
        name.setTextColor(context.getResources().getColor(R.color.item_text_color));
        SpannableString spanString = new SpannableString(this.name);
        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        name.setText(spanString);

        TextView optT = new TextView(context);
        optT.setLineSpacing(1f, 1.25f);
        optT.setPadding(70, 0, 0, 0);
        optT.setBackgroundColor(resources.getColor(R.color.item_name));
        optT.setTextColor(context.getResources().getColor(R.color.item_text_color));
        optT.setLayoutParams(params);

        for (String opt: content) {
            optT.append("\u2022   ");
            SpannableString sS = new SpannableString(opt);
            sS.setSpan(new StyleSpan(Typeface.ITALIC), 0, sS.length(), 0);
            optT.append(sS);
            optT.append("\n");
        }

        gl.addView(name);
        gl.addView(optT);
        return gl;
    }

}
